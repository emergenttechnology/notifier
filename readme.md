NOTIFIER

This is a simple project to make one of those "status" LED indicators.  I am well aware you can buy one for the same price, but where is the fun in that?

The first version is a very simple driver which monitors the serial line (USB) for a single byte command and sets the 3 LEDs to a suitable colour, which is defined in code.

I expect I will create a fork of the project which firstly maintains this version, but will allow me to create something using MQTT for better remote control.