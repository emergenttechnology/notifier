/*
 * NOTIFIER: Notifier is a visual display tool to show status of various systems
 * For example, Skype for business status.
 * 
 * Assumptions:
 * 
 *  There is a USB connection to the host machine. (in a future version, I might do either WiFi
 *  or Ethernet)
 *  Initial development was done on an Arduino Mega 2560, so the pin values are set up for that.
 *  PIN9  - Red
 *  PIN10 - Green
 *  PIN11 - Blue
 *  
 *  My LEDs are common-anode devices, so this complicates the code a little
 *  100% duty = OFF and 0% = ON with the varying degree in between.
 *  If you use a common-cathode device, you need to reverse the logic.
 */

const int RedLed = 9;
const int GreenLed = 10;
const int BlueLed = 11;
const int OnBoard = 13;

int msg = 0;

void OnSetLed()
{
  ledsOff();
  
  switch(msg) {
    case 0x30:
      setColour(RedLed,0);
      setColour(GreenLed,0);
      setColour(BlueLed,0);
    case 0x31:  // Busy
      setColour(RedLed, 0);
      break;
    case 0x32:  // Free
      setColour(GreenLed, 0);
      break;
    case 0x33:  // Away
      setColour(BlueLed, 0);
      break;
    case 0x34:  // On the Phone
      setColour(RedLed, 0);
      setColour(BlueLed, 128);
      break;
    case 0x35:  // Unknown
      setColour(GreenLed, 0);
      setColour(BlueLed, 128);
      break;
    default:
      ledsOff();  
  }
}

void setColour(int led, int colour)
{
  analogWrite(led, colour);
}

void ledsOff() {
  analogWrite(RedLed, 255);
  analogWrite(GreenLed, 255);
  analogWrite(BlueLed, 255); 
}
void setup() {
  // Set up our LEDs.  Change this for different boards
  pinMode(RedLed, OUTPUT);
  pinMode(GreenLed, OUTPUT);
  pinMode(BlueLed, OUTPUT);
  pinMode(OnBoard, OUTPUT);

  // Our USB input is a simple serial device.
  Serial.begin(9600);

  // Serial.write("Starting\r\n");
  // A little "Splash" to start up
  for (int pin = RedLed; pin <= BlueLed; pin++) {
    ledsOff();
    for (int pwr = 255; pwr >= 0; pwr--) {
      setColour(pin, pwr);
      delay(5);
    }
    delay(250);
    for(int pwr = 0; pwr <= 255; pwr++) {
      setColour(pin, pwr);
      delay(5);
    }
  }
}

void loop() {
  // Just for sanity, use the on-board LED as an "alive" indicator
  //
  digitalWrite(OnBoard, HIGH);
  delay(500);
  msg = Serial.read();
  if(msg != -1)
    OnSetLed();
  digitalWrite(OnBoard, LOW);
  delay(500);
}

